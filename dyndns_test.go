/*
yaddnsc-dyndns
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package dyndns

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
	"github.com/vargspjut/wlog"
)

const defaultIP = "100.0.0.0"
const envService = "DYN_SERVICE"
const envToken = "DYN_TOKEN"
const envLogin = "DYN_LOGIN"
const envHost = "DYN_HOST"

var http *resty.Client
var serviceHost string
var token string
var login string
var host string

func init() {
	serviceHost = os.Getenv(envService)
	login = os.Getenv(envLogin)
	host = os.Getenv(envHost)
	token = os.Getenv(envToken)
	http = resty.New()
}

func TestNoChange(t *testing.T) {
	resetDNS(host)
	req := request{
		ServiceHost:    serviceHost,
		Login:          login,
		Password:       token,
		Hosts:          []string{host},
		ServiceVersion: "v2",
		Ssl:            true,
	}
	cacheRoot := mustMkTempDir()
	defer os.RemoveAll(cacheRoot)

	sut := New(cacheRoot, wlog.DefaultLogger(), "", false)
	err := sut.Init()
	assert.Nil(t, err)
	enc, err := sut.Execute(mustEncode(req), net.ParseIP(defaultIP))
	assert.Nil(t, err)
	result := mustDecode(enc)
	assert.Equal(t, 1, len(result))
	assert.Equal(t, "NoChg", result[0].Status)
}

func TestChange(t *testing.T) {
	resetDNS(host)
	req := request{
		ServiceHost:    serviceHost,
		Login:          login,
		Password:       token,
		Hosts:          []string{host},
		ServiceVersion: "v2",
		Ssl:            true,
	}
	cacheRoot := mustMkTempDir()
	defer os.RemoveAll(cacheRoot)

	sut := New(cacheRoot, wlog.DefaultLogger(), "", false)
	err := sut.Init()
	assert.Nil(t, err)
	enc, err := sut.Execute(mustEncode(req), net.ParseIP("100.50.50.50"))
	assert.Nil(t, err)
	result := mustDecode(enc)
	assert.Equal(t, 1, len(result))
	assert.Equal(t, "Ok", result[0].Status)
}

func TestErr(t *testing.T) {
	resetDNS(host)
	req := request{
		ServiceHost:    serviceHost,
		Login:          login,
		Password:       token,
		Hosts:          []string{host},
		ServiceVersion: "v2",
		Ssl:            true,
	}
	cacheRoot := mustMkTempDir()
	defer os.RemoveAll(cacheRoot)

	sut := New(cacheRoot, wlog.DefaultLogger(), "", false)
	err := sut.Init()
	assert.Nil(t, err)
	enc, err := sut.Execute(mustEncode(req), net.ParseIP("299.400.15.633")) // invalid ip must cause Err response
	assert.Nil(t, err)
	result := mustDecode(enc)
	assert.Equal(t, 1, len(result))
	assert.Equal(t, "Err", result[0].Status)
}

func mustDecode(input []byte) response {
	resp := response{}
	err := json.Unmarshal(input, &resp)
	if err != nil {
		panic(err)
	}
	return resp
}

func mustEncode(input any) []byte {
	enc, err := json.Marshal(input)
	if err != nil {
		panic(err)
	}
	return enc
}

func resetDNS(host string) {
	url := fmt.Sprintf("https://%s/v3/update", serviceHost)
	req := http.R()
	req.SetQueryParam("myip", defaultIP)
	req.SetQueryParam("hostname", host)
	req.SetBasicAuth(login, token)
	resp, err := req.Get(url)
	if err != nil {
		panic(err)
	}
	body := string(resp.Body())
	if resp.StatusCode() != 200 || (body != "nochg" && body != "good") {
		panic(fmt.Errorf("dns reset failed: [%d/%s]", resp.StatusCode(), body))
	}
}

func mustMkTempDir() string {
	d, err := os.MkdirTemp("", "yaddnsc-dyn-*")
	if err != nil {
		panic(err)
	}
	return d
}
