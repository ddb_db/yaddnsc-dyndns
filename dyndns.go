/*
yaddnsc-dyndns
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package dyndns

import (
	"bufio"
	_ "embed"
	"encoding/json"
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc-dyndns/internal/cache"
)

const (
	unknownIP = "0.0.0.0"

	defaultServiceHost    = "members.dyndns.org"
	defaultServiceVersion = "v2"

	dyndnsErr   = "err"
	dyndnsNoChg = "nochg"
	dyndnsGood  = "good"

	yaddnscErr   = "Err"
	yaddnscNoChg = "NoChg"
	yaddnscOk    = "Ok"

	respOrigIp    = "OriginalIP"
	respUpdatedIp = "UpdatedIP"
	respHost      = "Host"
	respDynStatus = "DynStatus"
	respStatus    = "Status"
)

//go:embed schema.json
var schema []byte

type Processor struct {
	log           wlog.Logger
	http          *resty.Client
	cache         cache.Provider
	isHttpLogging bool
	userAgent     string
	cacheRoot     string
	initialized   bool
}

type request struct {
	ServiceHost    string
	ServiceVersion string
	Login          string
	Password       string
	Hosts          []string
	Ssl            bool
}

type responseEntry struct {
	Details     map[string]any `json:"details,omitempty"`
	Description string         `json:"description"`
	Status      string         `json:"status"`
}

type response []responseEntry

func New(cacheRoot string, log wlog.Logger, userAgent string, isHttpLogging bool) *Processor {
	return &Processor{
		log:           log,
		userAgent:     userAgent,
		cacheRoot:     cacheRoot,
		isHttpLogging: isHttpLogging,
		initialized:   false,
	}
}

func (impl *Processor) Init() error {
	if impl.initialized {
		return nil
	}
	impl.http = buildRestyClient(impl.userAgent, impl.isHttpLogging)
	impl.cache = cache.NewProvider(impl.cacheRoot, impl.log)
	impl.initialized = true
	return nil
}

func buildRestyClient(userAgent string, isHttpLogging bool) *resty.Client {
	c := resty.New()
	if len(userAgent) > 0 {
		c.SetHeader("User-Agent", userAgent)
	}
	if isHttpLogging {
		c.EnableTrace()
	}
	return c
}

func (impl *Processor) Describe() string {
	return "DynDNS Compatible Service"
}

func (impl *Processor) Schema() []byte {
	return schema
}

func (impl *Processor) Execute(blob []byte, systemIP net.IP) ([]byte, error) {
	ddns := request{
		ServiceHost:    defaultServiceHost,
		ServiceVersion: defaultServiceVersion,
		Ssl:            true,
	}
	if err := json.Unmarshal(blob, &ddns); err != nil {
		return nil, fmt.Errorf("yaddnsc-dyndns: unmarshal failed: %w", err)
	}

	r, url, hosts := impl.buildRequest(ddns, systemIP)
	if len(hosts) > 0 {
		resp, err := r.Get(url)
		if err != nil {
			return nil, fmt.Errorf("yaddnsc-dyndns: http error: %w", err)
		}
		impl.logHttpRequest(r, resp)
		if resp.Error() != nil {
			return nil, fmt.Errorf("yaddnsc-dyndns: http get failed: %v", resp.Error())
		}
		result := impl.parseResponse(string(resp.Body()), ddns, hosts, systemIP)
		return json.Marshal(result)
	} else {
		impl.log.Debug("dyndns: nothing to do, no ip changes detected")
		return json.Marshal(response{})
	}
}

func (impl *Processor) parseResponse(body string, ddns request, hosts []string, systemIP net.IP) response {
	impl.log.Debugf("dyndns: response received:\n%s", body)
	systemIPStr := unknownIP
	if systemIP != nil {
		systemIPStr = systemIP.String()
	} else {
		impl.log.Warning("received nil system IP")
	}
	resp := make(response, 0)
	idx := 0
	scanner := bufio.NewScanner(strings.NewReader(body))
	for scanner.Scan() {
		dynStatus := scanner.Text()
		e := impl.cache.Get(hosts[idx], systemIP)
		vars := map[string]any{
			respOrigIp:    e.Ip,
			respUpdatedIp: systemIPStr,
			respHost:      hosts[idx],
			respDynStatus: dynStatus,
		}
		entry := responseEntry{
			Description: fmt.Sprintf("dyndns-%s/%s", ddns.ServiceVersion, hosts[idx]),
		}
		err := impl.convertResult(dynStatus)
		if err != nil {
			vars[respStatus] = yaddnscErr
			entry.Status = yaddnscErr
		} else {
			if vars[respDynStatus] == dyndnsNoChg {
				vars[respStatus] = yaddnscNoChg
				entry.Status = yaddnscNoChg
			} else {
				vars[respStatus] = yaddnscOk
				entry.Status = yaddnscOk
			}
			e.Ip = systemIPStr
			e.LastUpdate = time.Now()
			impl.cache.UpdateHost(e)
		}
		entry.Details = vars
		resp = append(resp, entry)
		idx++
	}
	impl.log.Debugf("dyndns: parsed response:\n%+v", resp)
	return resp
}

func (impl *Processor) convertResult(result string) error {
	if result == dyndnsGood || result == dyndnsNoChg {
		return nil
	}
	impl.log.WithScope(wlog.Fields{"result": result}).Debug("dyndns: parsed result as error")
	return fmt.Errorf(result)
}

func (impl *Processor) buildRequest(ddns request, systemIP net.IP) (*resty.Request, string, []string) {
	hosts := impl.filterUnchanged(ddns.Hosts, systemIP)
	r := impl.http.R()
	r.SetBasicAuth(ddns.Login, ddns.Password)
	r.SetQueryParam("myip", systemIP.String())
	r.SetQueryParam("hostname", strings.Join(hosts, ","))
	scheme := "https"
	if !ddns.Ssl {
		scheme = "http"
	}
	var url string
	switch ddns.ServiceVersion {
	case "v2":
		r.SetQueryParam("wildcard", "NOCHG")
		r.SetQueryParam("mx", "NOCHG")
		r.SetQueryParam("backmx", "NOCHG")
		url = fmt.Sprintf("%s://%s/nic/update", scheme, ddns.ServiceHost)
	case "v3":
		url = fmt.Sprintf("%s://%s/v3/update", scheme, ddns.ServiceHost)
	default:
		panic(fmt.Errorf("yaddnsc-dyndns: unknown version [%s]", ddns.ServiceVersion))
	}
	impl.log.WithScope(wlog.Fields{"hosts": hosts, "url": url, "ver": ddns.ServiceVersion}).Debug("dyndns: request built")
	return r, url, hosts
}

func (impl *Processor) filterUnchanged(hosts []string, systemIP net.IP) []string {
	result := make([]string, 0)
	for _, h := range hosts {
		c := impl.cache.Get(h, systemIP)
		if !systemIP.Equal(net.ParseIP(c.Ip)) || c.IsHostStale() {
			if c.IsHostStale() {
				impl.log.WithScope(wlog.Fields{"host": h, "type": c.Type}).Debug("dyndns: forcing update; stale record")
			} else {
				impl.log.WithScope(wlog.Fields{"host": h, "type": c.Type}).Debug("dyndns: updating; IPs differ")
			}
			result = append(result, h)
		} else {
			impl.log.WithScope(wlog.Fields{"host": h, "type": c.Type}).Debug("dyndns: ignoring; IPs are same")
		}
	}
	return result
}

func (impl *Processor) logHttpRequest(req *resty.Request, resp *resty.Response) {
	if !impl.isHttpLogging {
		return
	}
	trace := resp.Request.TraceInfo()
	fields := wlog.Fields{
		"restyResult":        resp.Result(),
		"restyErr":           resp.Error(),
		"restyHttpCode":      resp.StatusCode(),
		"restyHttpStatus":    resp.Status(),
		"restyProto":         resp.Proto(),
		"restyDuration":      resp.Time().String(),
		"restyRecdAt":        resp.ReceivedAt(),
		"restyDnsLookup":     trace.DNSLookup.String(),
		"restyConnTime":      trace.ConnTime.String(),
		"restyTCPConnTime":   trace.TCPConnTime.String(),
		"restyTLSHandsTime":  trace.TLSHandshake.String(),
		"restyTTFB":          trace.ServerTime.String(),
		"restyRespTime":      trace.ResponseTime.String(),
		"restyTotalTime":     trace.TotalTime.String(),
		"restyIsConnReused":  trace.IsConnReused,
		"restyIsConnWasIdle": trace.IsConnWasIdle,
		"restyReqAttempt":    trace.RequestAttempt,
		"restyRemoteAddr":    trace.RemoteAddr.String(),
		"restyTarget":        resp.Request.RawRequest.URL.String(),
		"restyMethod":        resp.Request.Method,
		"restyAgent":         resp.Request.RawRequest.UserAgent(),
		"restyRespBody":      string(resp.Body()),
	}
	impl.log.WithScope(fields).Debug("resty request")
}
