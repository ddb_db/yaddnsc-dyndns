/*
yaddnsc-dyndns
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package cache

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
	"time"

	"github.com/vargspjut/wlog"
)

const (
	cacheFile       = "cache.json"
	cacheStaleHours = time.Hour * 24 * 28
	typeIPv4        = "IPv4"
	typeIPv6        = "IPv6"
)

type Provider interface {
	Get(host string, ip net.IP) *Entry
	UpdateHost(entry *Entry)
}

type cacheMap map[string]*Entry

type Entry struct {
	Host       string
	Type       string
	Ip         string
	LastUpdate time.Time
}

func (e *Entry) IsIPDifferent(ip string) bool {
	return e.Ip != ip
}

func (e *Entry) IsHostStale() bool {
	return e.LastUpdate.Add(cacheStaleHours).Before(time.Now())
}

type defaultProvider struct {
	fileName string
	cmap     cacheMap
	log      wlog.Logger
}

func (impl *defaultProvider) Get(host string, ip net.IP) *Entry {
	k := impl.genKey(host, ip)
	e, ok := impl.cmap[k]
	if !ok {
		impl.log.WithScope(wlog.Fields{"host": host, "ip": ip.String()}).Debug("cache: miss")
		e = &Entry{
			Type:       getIpType(ip),
			Host:       host,
			Ip:         "0.0.0.0",
			LastUpdate: time.Time{},
		}
	} else {
		impl.log.WithScope(wlog.Fields{"host": host, "ip": ip.String()}).Debug("cache: hit")
	}
	return e
}

func getIpType(ip net.IP) string {
	if ip.To4() != nil {
		return typeIPv4
	}
	return typeIPv6
}

func (impl *defaultProvider) UpdateHost(entry *Entry) {
	key := impl.genKey(entry.Host, net.ParseIP(entry.Ip))
	impl.cmap[key] = entry
	enc, err := json.Marshal(impl.cmap)
	if err != nil {
		impl.log.WithScope(wlog.Fields{"err": err.Error()}).Error("cache: marshal failed")
		return
	}
	if err := os.WriteFile(impl.fileName, enc, 0644); err != nil {
		impl.log.WithScope(wlog.Fields{"err": err.Error()}).Error("cache: write failed")
	} else {
		impl.log.WithScope(wlog.Fields{"entry": entry}).Debug("cache: flushed")
	}
}

func (impl *defaultProvider) genKey(host string, ip net.IP) string {
	ipType := typeIPv4
	if isIp4 := ip.To4() != nil; !isIp4 {
		ipType = typeIPv6
	}
	key := fmt.Sprintf("%s/%s", ipType, host)
	impl.log.WithScope(wlog.Fields{"host": host, "ip": ip.String(), "key": key}).Debug("cache: key generated")
	return key
}

func NewProvider(cacheRoot string, log wlog.Logger) Provider {
	name := fmt.Sprintf("%s/%s", cacheRoot, cacheFile)
	_, err := os.Stat(name)
	if os.IsNotExist(err) {
		if err := os.WriteFile(name, []byte{}, 0644); err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}

	blob, err := os.ReadFile(name)
	if err != nil {
		panic(err)
	}

	cmap := make(cacheMap)
	if len(blob) > 0 {
		err = json.Unmarshal(blob, &cmap)
		if err != nil {
			panic(err)
		}
	}

	return &defaultProvider{
		fileName: name,
		cmap:     cmap,
		log:      log,
	}
}
